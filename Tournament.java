import java.util.ArrayList;

enum Style{Singles, Doubles, Triples, Rotation, Royal}

public class Tournament {
    private ArrayList<Trainer> trainers;
    private Style style;
    private int minBadges;

    //Constructors
    public Tournament(){
        trainers = new ArrayList<Trainer>();
        style = Style.Singles;
        minBadges = 0;
    }

    public Tournament(ArrayList<Trainer> trainers, Style style, int minBadges){
        this.trainers = trainers;
        this.style = style;
        this.minBadges = minBadges;
    }

    //Getters
    public ArrayList<Trainer> getTrainers(){
        return trainers;
    }
    public Style getStyle(){
        return style;
    }
    public int getMinBadges(){
        return minBadges;
    }
    //Setters
    public void setTrainers(ArrayList<Trainer> trainers){
        this.trainers = trainers;
    }
    public void setStyle(Style style){
        this.style = style;
    }
    public void setMinBadges(int minBadges){
        this.minBadges = minBadges;
    }
    //Check qualifications. If a trainer doesnt meet the minimum requirements for the tournament, returns false.
    public boolean checkQualifications(Trainer t){
        if(t.getBadges() < minBadges){
            return false;
        }
        if(style == Style.Singles || style == Style.Royal){
            if(t.countTeam() < 3){
                return false;
            }
        } else if(style == Style.Doubles){
            if(t.countTeam() < 4){
                return false;
            }
        } else {
            if(t.countTeam() < 5){
                return false;
            }
        }
        return true;
    }

    //Add trainer. Checks if the trainer qualifies, if they do, they are added to the roster.
    public boolean addTrainer(Trainer t){
        if(checkQualifications(t)){
            trainers.add(t);
            return true;
        }
        return false;
    }

    //Remove trainer. Checks if a trainer is registered. If found, removes trainer
    public boolean removeTrainer(Trainer t){
        return trainers.remove(t);
    }

    
}
