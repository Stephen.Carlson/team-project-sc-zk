import java.util.ArrayList;

public class Trainer {
    //Variables
    private int ID;
    private String name;
    private int badges;
    private ArrayList<Pokemon> pc;
    private Pokemon[] team;
    private final int TEAM_SIZE = 6;


    //Constructors
    public Trainer(){
        ID = 123;
        name = "Ash Ketchum";
        badges = 0;
        pc = new ArrayList<Pokemon>();
        team = new Pokemon[TEAM_SIZE];
        for(int i = 0; i < TEAM_SIZE; i++){
            team[i] = new Pokemon();
        }
    }
    public Trainer(int ID, String name, int badges, ArrayList<Pokemon> pc, Pokemon[] team){
        this.ID = ID;
        this.name = name;
        this.badges = badges;
        this.pc = pc;
        if(team.length == TEAM_SIZE){
            this.team = team;
        } else {
            this.team = new Pokemon[TEAM_SIZE];
        }
    }
    //Getters
    public int getID(){
        return ID;
    }
    public String getName(){
        return name;
    }
    public int getBadges(){
        return badges;
    }
    public ArrayList<Pokemon> getPC(){
        return pc;
    }
    public Pokemon[] getTeam(){
        return team;
    }
    //Setters
    public void setID(int ID){
        this.ID = ID;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setBadges(int badges){
        this.badges = badges;
    }
    public void setPC(ArrayList<Pokemon> pc){
        this.pc = pc;
    }
    public void setTeam(Pokemon[] team){
        if(team.length == TEAM_SIZE){
            this.team = team;
        }
    }

    //Catch pokemon. Takes in a pokemon and adds it to the pc.
    public void catchPokemon(Pokemon p){
        pc.add(p);
    }

    //Release pokemon. Removes a pokemon from the PC if it is there
    public void releasePokemon(Pokemon p){
        pc.remove(p);
    }

    //Show team. Prints out the pokemon in the trainers team
    public void showTeam(){
        for(int i = 0; i < TEAM_SIZE; i++){
            System.out.print(i+1 + ": ");
            if(!(team[i].getName().equals("default"))){
                System.out.println(team[i].getName() + ", Type " + team[i].getT() + ", HP: " + team[i].getHp());
            } else {
                System.out.println("Empty");
            }
        }
    }

    //Show PC. Prints out the pokemon in the trainers PC
    public void showPC(){
        int count = 1;
        for (Pokemon p : pc){
            System.out.println(count + ": " + p.getName() + ", Type: " + p.getT() + ", HP: " + p.getHp());
        }
    }

    //Add to team. Searches through PC for specific pokemon, then if found adds it to the team in slot. Will swap out pokemon if one is already in that spot. Removes PC
    public boolean addToTeam(Pokemon p, int slot){
        if(pc.contains(p)){
            if(team[slot].getName().equals("default")){
                team[slot] = p;
                pc.remove(p);
            } else {
                pc.add(team[slot]);
                team[slot] = p;
                pc.remove(p);
            }
            return true;
        } else {
            return false;
        }
    }

    //Remove from team. If there is a pokemon in slot, then transfer the pokemon to the PC and replace spot with default
    public boolean removeFromTeam(int slot){
        if(team[slot].getName().equals("default")){
            return false;
        } else {
            pc.add(team[slot]);
            team[slot] = new Pokemon();
            return true;
        }
    }

    //Count Team. Returns the number of pokemon currently on the trainers team
    public int countTeam(){
        int count = 0;
        for(int i = 0; i < TEAM_SIZE; i++){
            if(!(team[i].getName().equals("default"))){
                count++;
            }
        }
        return count;
    }


    //This acts the same as addToTeam, except the pokemon is not then removed from the PC
    public boolean addToTeamIncorrect(Pokemon p, int slot){
        if(pc.contains(p)){
            if(team[slot].getName().equals("default")){
                team[slot] = p;
            } else {
                pc.add(team[slot]);
                team[slot] = p;
            }
            return true;
        } else {
            return false;
        }
    }
}