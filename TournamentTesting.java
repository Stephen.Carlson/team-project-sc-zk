import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.Matcher.*;

import java.util.ArrayList;

public class TournamentTesting {
    private static Tournament single;
    private static Tournament royal;
    private static Tournament triple;
    private static Trainer red;
    private static Trainer ash;
    private static Trainer misty;
    private static Trainer brock;
    private static Trainer gary;
    private static Trainer dawn;
    private static Pokemon bulbasaur;
    private static Pokemon pikachu;
    private static Pokemon pidgey;
    private static Pokemon psyduck;

    @BeforeClass
    public static void setUp(){
        single = new Tournament();
        triple = new Tournament(new ArrayList<Trainer>(), Style.Triples, 4);
        royal = new Tournament();

        pikachu = new Pokemon(Type.Electric, "Pikachu", 120);
        psyduck = new Pokemon(Type.Water, "Psyduck", 200);
        pidgey = new Pokemon(Type.Flying, "Pidgey", 20);
        bulbasaur = new Pokemon();

        ash = new Trainer();
        Pokemon[] mistyTeam = {new Pokemon(), new Pokemon(), new Pokemon(), new Pokemon(), new Pokemon(), new Pokemon()};
        misty = new Trainer(321, "Misty", 8, new ArrayList<Pokemon>(), mistyTeam);
        Pokemon[] brockTeam = {pidgey, pidgey, pidgey, new Pokemon(), new Pokemon(), new Pokemon()};
        brock = new Trainer(111, "Brock", 4, new ArrayList<Pokemon>(), brockTeam);
        Pokemon[] garyTeam = {new Pokemon(), new Pokemon(), new Pokemon(), new Pokemon(), new Pokemon(), new Pokemon()};
        gary = new Trainer(222, "Gary", 8, new ArrayList<Pokemon>(), garyTeam);
        Pokemon[] dawnTeam = {psyduck, psyduck, psyduck, psyduck, psyduck, psyduck};
        dawn = new Trainer(333, "Dawn", 2, new ArrayList<Pokemon>(), dawnTeam);
        red = new Trainer();
        
    }

    //This test consists of running all the getters and setters for them Pokémon,
    //Trainer, and Tournament classes to ensure they set or return the proper 
    //values. Success for this test is defined as the get and set methods setting
    // and then returning the proper values given in the code. Note, setTeam for 
    //the trainer class is run twice since the set has more logic in it to ensure
    // the team is of size 6. 
    @Test
    public void testGetSet(){
        bulbasaur.setHp(100);
        bulbasaur.setName("Bulbasaur");
        bulbasaur.setT(Type.Grass);
        Assert.assertEquals(100, bulbasaur.getHp());
        Assert.assertEquals("Bulbasaur", bulbasaur.getName());
        Assert.assertEquals(Type.Grass, bulbasaur.getT());

        red.setID(999);
        red.setBadges(8);
        red.setName("Red");
        Pokemon[] tempTeam = new Pokemon[6];
        tempTeam[0] = bulbasaur;
        red.setTeam(tempTeam);
        ArrayList<Pokemon> tempPC = new ArrayList<Pokemon>();
        tempPC.add(pikachu);
        red.setPC(tempPC);
        Assert.assertEquals(999, red.getID());
        Assert.assertEquals(8, red.getBadges());
        Assert.assertEquals("Red", red.getName());
        Assert.assertEquals(bulbasaur, red.getTeam()[0]);
        Assert.assertEquals(pikachu, red.getPC().get(0));

        Pokemon[] badTeam = new Pokemon[5];
        badTeam[0] = pikachu;
        red.setTeam(badTeam);
        Assert.assertEquals(bulbasaur, red.getTeam()[0]);

        royal.setMinBadges(4);
        royal.setStyle(Style.Royal);
        ArrayList<Trainer> tempTrainers = new ArrayList<Trainer>();
        tempTrainers.add(red);
        royal.setTrainers(tempTrainers);

        Assert.assertEquals(4, royal.getMinBadges());
        Assert.assertEquals(Style.Royal, royal.getStyle());
        Assert.assertEquals(red, royal.getTrainers().get(0));

    }


    //This test consists of testing the catchPokemon and releasePokemon 
    //functions located in the 	Trainer class. These functions ensure that 
    //Pokemon are added or removed from the Trainers 	PC when they are caught 
    //or released respectively. This test is successful when a Pokemon is 	
    //added to the players PC when caught and removed from the PC when released.
    @Test
    public void testCatchandRelease(){
        ash.catchPokemon(pikachu);
        Assert.assertEquals(pikachu, ash.getPC().get(0));

        ash.catchPokemon(pidgey);
        Assert.assertEquals(pidgey, ash.getPC().get(1));
        Assert.assertEquals(pikachu, ash.getPC().get(0));

        ash.releasePokemon(pikachu);
        Assert.assertEquals(pidgey, ash.getPC().get(0));
    }


    //This test evaluates whether the changeHp functions correctly. 
    //This test succeeds when a given pokemons HP increases when a positive
    // number is passed to the function, and decreases 	when a negative number
    // is passed.
    @Test
    public void testHP(){
        pikachu.changeHp(-20);
        Assert.assertEquals(100, pikachu.getHp());

        pikachu.changeHp(-400);
        Assert.assertEquals(0, pikachu.getHp());
    }


    //This test evaluates the team methods in the Trainer class. These functions are designed to not 
    //let a trainer add a Pokemon to their team if it is not already in their PC, and will also put the 
    //Pokemon back into the Trainers PC if removed from the team. Also, the countTeam method 
    //counts the number of  Pokemon currently on the trainer's team. This test succeeds when a 
    //Pokemon already in the PC is added to the team, a Pokemon not currently in the PC is not added 
    //to the team, and the countTeam function return the proper number of Pokemon on the team.
    @Test
    public void testTeam(){
        misty.catchPokemon(pikachu);
        misty.catchPokemon(pidgey);

        misty.addToTeam(pikachu, 0);
        Assert.assertEquals(pikachu, misty.getTeam()[0]);

        misty.addToTeam(pidgey, 1);
        Assert.assertEquals(pidgey, misty.getTeam()[1]);
        Assert.assertEquals(2, misty.countTeam());

        misty.addToTeam(psyduck, 1);
        Assert.assertEquals(pidgey, misty.getTeam()[1]);

        misty.removeFromTeam(1);
        Assert.assertEquals("default", misty.getTeam()[1].getName());
        Assert.assertEquals(pidgey, misty.getPC().get(0));
        Assert.assertEquals(1, misty.countTeam());

        misty.addToTeam(pidgey, 0);
        Assert.assertEquals(pidgey, misty.getTeam()[0]);
        Assert.assertEquals(pikachu, misty.getPC().get(0));
        Assert.assertEquals(1, misty.countTeam());

    }


    //This test evaluates the Tournament classes functions. The checkQualifications method restricts
    // a trainers ability to enter a torunament if they do not have a minimum number of badges, or minimum
    // number of Pokemon on their team for the style of tournament. This test succeeds when a trainer without
    // proper qualifications is not allowed to be entered into the tournament, or one with proper 
    //qualifications is allowed to enter. 
    @Test
    public void testTournament(){
        single.addTrainer(brock);
        Assert.assertEquals(brock, single.getTrainers().get(0));

        single.addTrainer(gary);
        Assert.assertEquals(brock, single.getTrainers().get(0));

        for(int i = 0; i < 5; i++){
            gary.catchPokemon(pikachu);
            gary.addToTeam(pikachu, i);
        }

        triple.addTrainer(gary);
        Assert.assertEquals(gary, triple.getTrainers().get(0));

        triple.addTrainer(brock);
        Assert.assertEquals(1, triple.getTrainers().size());

        triple.addTrainer(dawn);
        Assert.assertEquals(1, triple.getTrainers().size());
        
    }

    //This is the incorrect version of the testTeam code. It uses the addToTeamIncorrect method instead of the
    //correct version.
    @Test
    public void testTeamIncorrect(){
        misty.catchPokemon(pikachu);
        misty.catchPokemon(pidgey);

        misty.addToTeamIncorrect(pikachu, 0);
        Assert.assertEquals(pikachu, misty.getTeam()[0]);

        misty.addToTeamIncorrect(pidgey, 1);
        Assert.assertEquals(pidgey, misty.getTeam()[1]);
        Assert.assertEquals(2, misty.countTeam());

        misty.addToTeamIncorrect(psyduck, 1);
        Assert.assertEquals(pidgey, misty.getTeam()[1]);

        misty.removeFromTeam(1);
        Assert.assertEquals("default", misty.getTeam()[1].getName());
        Assert.assertEquals(pidgey, misty.getPC().get(0));
        Assert.assertEquals(1, misty.countTeam());

        misty.addToTeamIncorrect(pidgey, 0);
        Assert.assertEquals(pidgey, misty.getTeam()[0]);
        Assert.assertEquals(pikachu, misty.getPC().get(0));
        Assert.assertEquals(1, misty.countTeam());

    }
}
