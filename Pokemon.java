enum Type {Normal, Fire, Water, Grass, Electric, Ice, FIghting, Poison, Ground, Flying, Psychic, Bug, Rock, Ghost, Dark, Dragon, Steel, Fairy}

public class Pokemon {
    private Type t;
    private String name;
    private int hp;
    //Constructors
    public Pokemon(){
        t = Type.Normal;
        name = "default";
        hp = -1;
    }
    public Pokemon(Type t, String n, int h){
        this.t = t;
        name = n;
        hp = h;
    }
    //getters/setters
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Type getT() {
        return t;
    }
    public void setT(Type t) {
        this.t = t;
    }
    public int getHp() {
        return hp;
    }
    public void setHp(int hp) {
        this.hp = hp;
    }
    //other functions
    public void changeHp(int n){
        hp += n;
        if(hp < 0){
            hp = 0;
        }
    }
}


