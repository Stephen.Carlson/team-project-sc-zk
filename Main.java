import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        Tournament single = new Tournament();
        single.setMinBadges(2);

        Pokemon pikachu = new Pokemon(Type.Electric, "Pikachu", 100);
        Pokemon squirtle = new Pokemon(Type.Water, "Squirtle", 120);
        Pokemon bulbasuar = new Pokemon(Type.Grass, "Bulbasaur", 140);
        Pokemon charmander = new Pokemon(Type.Fire, "Charmander", 100);

        Trainer user = new Trainer();
        System.out.println("Welcome to the world of Pokemon! First, what's you're name?");
        String name = in.nextLine();
        user.setName(name);

        System.out.println("Welcome, " + user.getName() + ", now its time to pick your starter!");
        System.out.println("Choose between these 4 pokemon, typing their full name as seen to choose it.");
        System.out.println(pikachu.getName() + ", The " + pikachu.getT() + " type Pokemon");
        System.out.println(bulbasuar.getName() + ", The " + bulbasuar.getT() + " type Pokemon");
        System.out.println(squirtle.getName() + ", The " + squirtle.getT() + " type Pokemon");
        System.out.println(charmander.getName() + ", The " + charmander.getT() + " type Pokemon");
        boolean flag = false;
        while(!flag){
            String choice = in.nextLine();
            if(choice.equals("Pikachu")){
                user.catchPokemon(pikachu);
                user.addToTeam(pikachu, 0);
                flag = true;
            } else if(choice.equals("Bulbasaur")){
                user.catchPokemon(bulbasuar);
                user.addToTeam(bulbasuar, 0);
                flag = true;
            } else if(choice.equals("Squirtle")){
                user.catchPokemon(squirtle);
                user.addToTeam(squirtle, 0);
                flag = true;
            } else if(choice.equals("Charmander")){
                user.catchPokemon(charmander);
                user.addToTeam(charmander, 0);
                flag = true;
            } else {
                System.out.println("Please type a valid name as seen above");
            }
        }
        System.out.println("Excelent choice! You two should enter the tournament happening soon!");

        boolean entered = single.addTrainer(user);
        if(entered){
            System.out.println("Congrats! You're added to the tournament");
        } else {
            System.out.println("Sorry, you're going to need " + single.getMinBadges() + " badges to enter the tournament");
        }

        in.close();
    }
}
